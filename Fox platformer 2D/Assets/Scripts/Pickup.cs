﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{

    public bool isGem, isHeal;

    private bool isColledted;

    public GameObject pickupEffect;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !isColledted)
        {
            if (isGem)
            {
                LevelManager.instance.gemsCollected++;

                isColledted = true;


                Destroy(gameObject);


                Instantiate(pickupEffect, transform.position, transform.rotation);

                UIController.instance.UpDateGemCount();

                AudioManager.instance.PlaySFX(6);

            }

            if (isHeal)
            {
                if (PlayerHealthController.instance.currentHealth != PlayerHealthController.instance.maxHealth)
                {
                    PlayerHealthController.instance.HealPlayer();
                    isColledted = true;
                    Destroy(gameObject);

                    Instantiate(pickupEffect, transform.position, transform.rotation);

                    AudioManager.instance.PlaySFX(7);

                }
            }
        }
    }
}
