﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    private void Awake()
    {
        instance = this;
    }


    public float moveSpeed;
    [HideInInspector]
    public Rigidbody2D theRb;
    public float jumpForce;

    private bool isGrounded;
    public Transform groundCheckPoint;
    public LayerMask whatIsGround;

    private bool canDubleJump;

    private Animator anim;
    private SpriteRenderer theSR;

    public float konckBackLength, knockBackForce;
    private float knockBackCounter;

    public float bounceForce;


    public bool stopInput;

    // Start is called before the first frame update
    void Start()
    {
        theRb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        theSR = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (!PauseMenu.instance.isPaused && !stopInput)
        {


            if (knockBackCounter <= 0)
            {


                theRb.velocity = new Vector2(moveSpeed * Input.GetAxis("Horizontal"), theRb.velocity.y);

                isGrounded = Physics2D.OverlapCircle(groundCheckPoint.position, 0.2f, whatIsGround);

                if (isGrounded)
                {
                    canDubleJump = true;
                }

                if (Input.GetButtonDown("Jump"))
                {
                    if (isGrounded)
                    {

                        AudioManager.instance.PlaySFX(10);
                        theRb.velocity = new Vector2(theRb.velocity.x, jumpForce);


                    }
                    else
                    {

                        if (canDubleJump)
                        {
                            theRb.velocity = new Vector2(theRb.velocity.x, jumpForce);
                            canDubleJump = false;
                            AudioManager.instance.PlaySFX(10);
                        }
                    }
                }

                if (theRb.velocity.x < 0)
                {
                    theSR.flipX = true;
                }

                else if (theRb.velocity.x > 0)
                {
                    theSR.flipX = false;
                }

            }
            else
            {
                knockBackCounter -= Time.deltaTime;
                if (!theSR.flipX)
                {
                    theRb.velocity = new Vector2(-knockBackForce, theRb.velocity.y);
                }
                else
                {
                    theRb.velocity = new Vector2(knockBackForce, theRb.velocity.y);
                }

            }
        }

            anim.SetFloat("moveSpeed", Mathf.Abs(theRb.velocity.x));
            anim.SetBool("isGrounded", isGrounded);
        
    }

    public void KnockBack()
    {
        knockBackCounter = konckBackLength;
        theRb.velocity = new Vector2(0, knockBackForce);
        anim.SetTrigger("hurt");
    }
    public void Bounce()
    {
        theRb.velocity = new Vector2(theRb.velocity.x, bounceForce);
        AudioManager.instance.PlaySFX(10);
    }

}
