﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;

    private void Awake()
    {
        instance = this;
    }

    public Transform target;

    public Transform farBackground, middleBackground;

    public float minHeight, maxHeight;

    public bool stopFollow;

  //  private float lastXPos;
     Vector2  lastPos;

    private void Start()
    {
        //  lastXPos = transform.position.x;
        //  lastYPos = transform.position.y;

        lastPos = transform.position;
    }

    private void Update()
    {

        /*    transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);

              float cmapedY = Mathf.Clamp(transform.position.y, minHeight, maxHeight);
              transform.position = new Vector3(transform.position.x, cmapedY, transform.position.z);


        //  float amountToMoveX = transform.position.x - lastXPos;
        //  float amountToMoveY = transform.position.y - lastYPos;

          transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, -1.5f, 2.5f),transform.position.z);
          transform.position = new Vector3(Mathf.Clamp(transform.position.x, -1.5f, 2.5f), transform.position.y, transform.position.z);


          farBackground.position = farBackground.position + new Vector3(amountToMoveX, 0f,0f);
          farBackground.position = farBackground.position + new Vector3(0f, amountToMoveY, 0f);


          middleBackground.position += new Vector3((amountToMoveX *0.5f), 0f, 0f);
          middleBackground.position += new Vector3((amountToMoveY * 0.5f), 0f, 0f);

          lastXPos = transform.position.x;
          lastYPos = transform.position.y;
          */
        if (!stopFollow)
        {



            transform.position = new Vector3(target.position.x, Mathf.Clamp(target.position.y, minHeight, maxHeight), transform.position.z);

            Vector2 amountToMove = new Vector2(transform.position.x - lastPos.x, transform.position.y - lastPos.y);

            farBackground.position = farBackground.position + new Vector3(amountToMove.x, amountToMove.y, 0f);
            middleBackground.position += new Vector3(amountToMove.x, amountToMove.y, 0f) * .5f;

            lastPos = transform.position;

        }


    }
}
