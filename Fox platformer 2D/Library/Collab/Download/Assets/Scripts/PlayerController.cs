﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed;
    private Rigidbody2D theRb;
    public float jumpForce;

    private bool isGrounded;
    public Transform groundCheckPoint;
    public LayerMask whatIsGround;

    private bool canDubleJump;

    private Animator anim;
    private SpriteRenderer theSR;


    // Start is called before the first frame update
    void Start()
    {
        theRb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        theSR = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        theRb.velocity = new Vector2(moveSpeed * Input.GetAxis("Horizontal"), theRb.velocity.y);

        isGrounded = Physics2D.OverlapCircle(groundCheckPoint.position, 0.2f,whatIsGround);

        if (isGrounded)
        {
            canDubleJump = true;
        }

        if (Input.GetButtonDown("Jump"))
        {
            if (isGrounded)
            {

                theRb.velocity = new Vector2(theRb.velocity.x, jumpForce);
                

            }
            else
            {

                if (canDubleJump)
                {
                    theRb.velocity = new Vector2(theRb.velocity.x, jumpForce);
                    canDubleJump = false;
                }
            }
        }

        if(theRb.velocity.x < 0)
        {
            theSR.flipX = true;
        }

        else if(theRb.velocity.x > 0)
        {
            theSR.flipX = false;
        }


        anim.SetFloat("moveSpeed", Mathf.Abs(theRb.velocity.x));
        anim.SetBool("isGrounded", isGrounded);
    }
}
