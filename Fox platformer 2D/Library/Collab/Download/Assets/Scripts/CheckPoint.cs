﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    SpriteRenderer sr;
    public Sprite cpOn, cpOff;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            CheckPointController.instance.DeactivateCheckpoints();


            sr.sprite = cpOn; 
        }
    }

    public void RestartCheeckPoint()
    {
        sr.sprite = cpOff;
    }
}
